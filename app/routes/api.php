<?php
/*  API file
*   Store general routes here
*/

use CosmicFramework\API\CosmicAccounts;
use CosmicFramework\MVC\View;
use WideImage\WideImage;

$klein->respond('/api/v1/upload', function () {
    require(__DIR__."/../../config.php");
    require_once(__DIR__."/../utils/id.php");
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if (in_array($ip, $config['application']['verified_ips'])) {

        $file_type = $_GET['file_type'];
        $service_name = $_GET['service_name'];
        foreach ($config['application']['cdn_services'] as $key => $val) {
            if(isset($val['custom_subdomain']) && $val['custom_subdomain'] == $_SERVER['HTTP_HOST']){
                $service_name = $key;
            }
        }
        if (isset($config['application']['cdn_services'][$service_name])) {
            switch ($file_type) {
                case "image":
                    $param = $_GET['param'];
                    if (in_array($param, $config['application']['cdn_services'][$service_name]['file_types']['images'])) {
                        $id = generateID(20);
                        $upload_dir = $config['application']['cdn_services'][$service_name]['directories'][0]."/images/".$param."/";
                        if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_dir . $id . ".jpg")) {
                            $success = "true";
                        }
                    }
                    if ($success == "true") {
                        echo(json_encode(['status' => 'success', 'id' => $id]));
                    } else {
                        echo(json_encode(['status' => 'error', 'error' => 'upload_failed']));
                    }
                    break;
                case "video":
                    break;
            }
    }

    }
    exit;
});

$klein->respond('/api/v1/delete', function () {
    require(__DIR__."/../../config.php");
    require_once(__DIR__."/../utils/id.php");
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if (in_array($ip, $config['application']['verified_ips'])) {

        $file_type = $_GET['file_type'];
        $service_name = $_GET['service_name'];
        foreach ($config['application']['cdn_services'] as $key => $val) {
            if(isset($val['custom_subdomain']) && $val['custom_subdomain'] == $_SERVER['HTTP_HOST']){
                $service_name = $key;
            }
        }
        $param = $_GET['param'];
        $file_id = $_GET['id'];
        if (isset($config['application']['cdn_services'][$service_name])) {
            if (isset($config['application']['cdn_services'][$service_name]['file_types'][$file_type])) {
                if (in_array($param, $config['application']['cdn_services'][$service_name]['file_types'][$file_type])) {
                    unlink($config['application']['cdn_services'][$service_name]['directories'][0]."/".$file_type."/".$param."/".$file_id.".jpg");
                } else {
                    unlink($config['application']['cdn_services'][$service_name]['directories'][0]."/".$file_type."/".$file_id.".jpg");
                }
            }
    }

    }
    exit;
});


$klein->respond('/setup', function () {
    require(__DIR__."/../../config.php");
    foreach ($config['application']['cdn_services'] as $service) {
            foreach ($service['directories'] as $dir_root) {
                foreach ($service['file_types'] as $key => $val) {
                    switch ($key) {
                        case "videos":
                            foreach ($val as $quality) {
                                $dir = $dir_root."/videos/".$quality."/";
                                if (!file_exists($dir) && !is_dir($dir)) {
                                    mkdir($dir, 0777, true);
                                    echo($dir_root.': Created '.$dir." <br>");
                                }
                            }
                            break;
                        case "images":
                            foreach ($val as $images) {
                                $dir = $dir_root."/images/".$images."/";
                                if (!file_exists($dir) && !is_dir($dir)) {
                                    mkdir($dir, 0777, true);
                                    echo($dir_root.': Created '.$dir." <br>");
                                }
                            }
                            break;
                        case "js":
                            $dir = $dir_root."/js/";
                            if (!file_exists($dir) && !is_dir($dir)) {
                                mkdir($dir, 0777, true);
                                echo($dir_root.': Created '.$dir." <br>");
                            }
                            break;
                        case "css":
                            $dir = $dir_root."/css/";
                            if (!file_exists($dir) && !is_dir($dir)) {
                                mkdir($dir, 0777, true);
                                echo($dir_root.': Created '.$dir." <br>");
                            }
                            break;
                    }
                }
            }
    }
    exit;
});

$klein->respond('/api/v1/img', function () {
    require(__DIR__.'/../../config.php');
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Content-Type: image/jpeg");
    header("Expires: 0"); // Proxies.
    require(__DIR__."/../../config.php");
    $service_name = $_GET['service_name'];

    foreach ($config['application']['cdn_services'] as $key => $val) {
        if(isset($val['custom_subdomain']) && $val['custom_subdomain'] == $_SERVER['HTTP_HOST']){
            $service_name = $key;
        }
    }

    if (isset($config['application']['cdn_services'][$service_name])) {
        $param = $_GET['c'];
        foreach ($config['application']['cdn_services'][$service_name]['file_types']['images'] as $key) {
            if ($key == $param) {
                $file = $config['application']['cdn_services'][$service_name]['directories'][0].'/images/'.$key."/".$_GET['id'].".jpg";
            }
        }

        if (isset($_GET['h']) || isset($_GET['w'])) {
            if (isset($_GET['h'])) {
                $h = $_GET['h'];
            } else {
                $h = "100%";
            }

            if (isset($_GET['w'])) {
                $w = $_GET['w'];
            } else {
                $w = "100%";   
            }

            $img = WideImage::LoadFromFile($file);
            if (isset($_GET['f']) && $_GET['f'] == "resize") {
                $img = $img->resize($w, $h);
            } elseif (isset($_GET['f']) && $_GET['f'] == "crop") {
                $img = $img->crop('center', 'center', $w, $h);
            } elseif (isset($_GET['f']) && $_GET['f'] == "fill") {
                $img = $img ->resize($w, $h, 'outside', 'any')
                ->crop('center', 'center', $w, $h);
            } elseif (isset($_GET['f']) && $_GET['f'] == "canvas") {
                $white = $img->allocateColor(255, 255, 255);
                $img = $img->resize("100%", $h, 'inside', 'any');
                $img = $img->resizeCanvas($w, $h, 'center', 0, $white);
            }
            echo($img->asString('jpg', 90));
        }
}
});

$klein->respond('/debug/my_ip', function () {

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

echo $ip;
    exit;
});
<?php
/*  Cosmic CDN
*   (c) CosmicMedia 2018
*/

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(E_ERROR | E_PARSE);

use CosmicFramework\MVC\Database;

require("config.php");
if (isset($_POST['session_key'])) {
    session_id($_POST['session_key']);
}
session_start();

Database::init($config["application"]["database"]["host"], $config["application"]["database"]["database"], $config["application"]["database"]["username"], $config["application"]["database"]["password"]);

// create instance of Klein, the routing package
$klein = new \Klein\Klein();

// Include routing files, contains definitions for the routes of the application
require("app/routes/api.php");

// Dispatch routes
$klein->dispatch();
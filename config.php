<?php
/*  Configuration file
*   Store general configuration
*/

$config = [
    "application" => [
        "verified_ips" => ["::1"],
        "cdn_services" => [
            "vanillo" => [
                "file_types" => ["images" => ['avatar', 'cover', 'thumbnail'], "videos" => [240, 480, 720, 1080, 1440, 2160], "js", "css"],
                "directories" => ["/var/www/cdn/vanillo"],
                "custom_subdomain" => "cdn.vanillo.co"
            ],
            "quizel" => [
                "file_types" => ["images" => ['avatar', 'cover', 'question'], "js", "css"],
                "directories" => ["/var/www/cdn/quizel"],
                "custom_subdomain" => "cdn.quizel.co"
            ]
            ]
    ]
];